package com.todomvc.thirdlesson;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Screenshots;
import com.codeborne.selenide.SelenideElement;
import com.google.common.io.Files;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TakesScreenshot;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.File;
import java.io.IOException;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

/**
 * Created by yulika on 7/19/15.
 */
public class TodoMVCTestAllure {

    @Before
    public void setUp() throws Exception {
        open("http://todomvc.com/examples/troopjs_require/");
    }

    @After
    public void tearDown() throws Exception {
        executeJavaScript("localStorage.clear()");
        screenshot();
    }

    @Test
    public void testAtAllTab(){
        //add tasks
        addTask("a");
        addTask("b");
        addTask("c");
        addTask("d");
        assertVisibleTasks("a", "b", "c", "d");
        assertItemsLeftCounter(4);

        //delete task
        deleteTask("b");
        assertVisibleTasks("a", "c", "d");
        assertItemsLeftCounter(3);

        //complete task and delete
        toggleTask("d");
        assertItemsLeftCounter(2);
        clearCompleted();
        assertVisibleTasks("a", "c");

        //cancel edit
        startEditing("a");
        inputNewValue("edited a").sendKeys(Keys.ESCAPE);
        assertVisibleTasks("a", "c");

        //edit task
        editTask("c", "edited c");
        assertVisibleTasks("a", "edited c");

        //reopen task
        toggleTask("edited c");
        assertItemsLeftCounter(1);
        toggleTask("edited c");
        assertItemsLeftCounter(2);

        //check all as completed and delete
        toggleAll();
        assertItemsLeftCounter(0);
        clearCompleted();
        assertVisibleTasks(empty);
    }

    @Test
    public void testAtActiveTab(){
        addTask("a");
        //add tasks from Active tab
        filterActive();
        addTask("b");
        addTask("c");
        addTask("d");
        assertVisibleTasks("a", "b", "c", "d");
        assertItemsLeftCounter(4);

        //complete task
        toggleTask("d");
        assertItemsLeftCounter(3);
        assertVisibleTasks("a", "b", "c");

        //check tasks on All tab
        filterAll();
        assertVisibleTasks("a", "b", "c", "d");
        filterActive();

        //delete by edit task
        editTask("c", "");
        assertVisibleTasks("a", "b");

        //check all as completed
        toggleAll();
        assertItemsLeftCounter(0);
        assertVisibleTasks(empty);

        //undo completed tasks
        toggleAll();
        assertItemsLeftCounter(3);
        assertVisibleTasks("a", "b", "d");
    }

    @Test
    public void testAtCompletedTab(){
        addTask("a");
        addTask("b");
        addTask("c");
        addTask("d");
        assertVisibleTasks("a", "b", "c", "d");

        //mark second as done
        toggleTask("b");
        assertItemsLeftCounter(3);

        //check task on completed tab
        filterComleted();
        assertVisibleTasks("b");

        //mark all as completed on active tab
        filterActive();
        toggleAll();
        assertItemsLeftCounter(0);
        assertVisibleTasks(empty);

        //check tasks on completed tab
        filterComleted();
        assertVisibleTasks("a", "b", "c", "d");

        //delete task
        deleteTask("b");
        assertVisibleTasks("a", "c", "d");
        assertItemsLeftCounter(0);

        //clear completed
        clearCompleted();
        assertVisibleTasks(empty);
    }

    public SelenideElement activeTasksCounter = $("#todo-count>strong");
    public ElementsCollection tasks = $$("#todo-list li");

    @Step
    public void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    public void addTask(String taskName) {
        $("#new-todo").setValue(taskName).pressEnter();
    }

    @Step
    public void clearCompleted() {
        $("#clear-completed").click();
    }

    @Step
    public void deleteTask(String taskValue) {
        tasks.findBy(text(taskValue)).hover().find(".destroy").click();
    }

    @Step
    public void toggleTask(String taskValue) {
        tasks.findBy(text(taskValue)).find(".toggle").click();
    }

    @Step
    public void filterAll(){
        filter("All");
    }

    @Step
    public void filterActive(){
        filter("Active");
    }

    @Step
    public void filterComleted(){
        filter("Completed");
    }

    @Step
    public void editTask(String taskForEdit, String newValue) {
        startEditing(taskForEdit);
        inputNewValue(newValue).pressEnter();
    }

    @Attachment(type = "image/png")
    public byte[] screenshot() throws IOException {
        File screenshot = Screenshots.getScreenShotAsFile();
        return Files.toByteArray(screenshot);
    }

    public void assertVisibleTasks(String... tasksName) {
        tasks.filter(visible).shouldHave(exactTexts(tasksName));
    }

    public void assertVisibleTasks(CollectionCondition isEmpty) {
        tasks.filter(visible).shouldBe(isEmpty);
    }

    public void assertItemsLeftCounter(int number) {
        activeTasksCounter.shouldHave(exactText(String.valueOf(number)));
    }

    private void filter(String tabName) {
        $(By.linkText(tabName)).click(); ;
        $("#filters .selected").shouldHave(exactText(tabName));
    }

    private SelenideElement inputNewValue(String newValue) {
        return tasks.findBy(cssClass("editing")).find(".edit").setValue(newValue);
    }

    private void startEditing(String taskForEdit) {
        tasks.findBy(text(taskForEdit)).find("label").doubleClick();
    }

}
