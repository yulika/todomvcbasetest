package com.todomvc.fourthlesson;

import org.junit.After;
import org.junit.Before;

import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by yulka on 10/9/15.
 */
public class OpenTodoMvcAndClearDataPerTest extends CapturingScreenshotPerTest {
    @Before
    public void setUp() {
        open("http://todomvc.com/examples/troopjs_require/");
    }

    @After
    public void clearData() {
        executeJavaScript("localStorage.clear()");
    }
}
