package com.todomvc.firstlesson;

import com.codeborne.selenide.ElementsCollection;
import org.junit.Test;

import static com.codeborne.selenide.CollectionCondition.*;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

/**
 * Created by yulika on 7/19/15.
 */
public class TodoMVCTest {

    @Test
    public void testHomeTask1(){
        open("http://todomvc.com/examples/troopjs_require/");
        addTask("1");
        addTask("2");
        addTask("3");
        addTask("4");
        tasks().shouldHave(exactTexts("1", "2", "3", "4"));

        tasks().findBy(text("2")).hover().find(".destroy").click();
        tasks().shouldHave(texts("1", "3", "4"));

        tasks().findBy(text("4")).find(".toggle").click();
        clearCompleted();
        tasks().shouldHave(texts("1", "3"));

        $("#toggle-all").click();
        clearCompleted();
        tasks().shouldBe(empty);
    }

    private void addTask(String taskName) {
        $("#new-todo").setValue(taskName).pressEnter();
    }

    private void clearCompleted() {
        $("#clear-completed").click();
    }

    public ElementsCollection tasks(){
        return $$("#todo-list li");
    }
}
